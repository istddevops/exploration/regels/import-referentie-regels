# Import Referentie Regels

Verkenning importeren van iStandaard Referentie-regels om deze te converteren naar Markdown met Front Matter variabelen. 

Via de volgende stappen kan de verkenning worden geevalueerd:

1. Uploaden van Excel-exports in .XLSX-formaat naar de folder [Bizz Regel Exports](bizz-regel-exports)

2. De import kan worden gestart via [GitLab Web Pipeline Interface](https://gitlab.com/istddevops/exploration/regels/import-referentie-regels/-/pipelines)

3. Daarna kan het resultaat worden geraadpleegd via de gegenereerde [HUGO Book Theme Pages Site](https://istddevops.gitlab.io/exploration/regels/import-referentie-regels)

## Doelstelling verkenning

Doel is het achterhalen of de huidige iStandaard Referentie-regels kunnen worden geconverteerd naar een generiek herbruikbaar formaat waarmee publicaties kunnen worden generereerd.

## Implementatie verkenning

De verkenning bouwt voort op de resultaten van de eerste uitgebrachte publicatie van de Referentieregels. De volgende onderdelen zijn daarbij toegepast en/of ontwikkeld:

- Maatwerk op basis dit [Python-script](bizregelstohugomd.py) 



