#
# Convert BizzDesign iStandaarden Regels export sheets to HUGO Content Markdown format
#

# packageimports
from openpyxl import load_workbook
import os
from humps import camelize

# iStandaard Regel Class
class IstdRegel:
    def __init__(self):
        self.code: str = None
        self.name: str = None
        self.documentatie = None
        self.wordtGebruiktIn = None
        self.geenTechnischeRegels = None
        self.XSDString = None
        self.rationale = None

# build HUGO Markdown content / docs path for regelType
def docsRegelPath(regelType: str):
    return os.path.join(os.getcwd(), 'content', 'docs', regelType.lower())

# return string between single quotes
def txtValue(inputStr: str):
    if inputStr != None:
        return inputStr
    else:
        return ""

# write HUGO Markdown content Index File for regeType
def writeIndexFile(regelType: str):
    regelPath = docsRegelPath(regelType)
    os.makedirs(regelPath, exist_ok=True)
    indexFilePath = os.path.join(regelPath, '_index.md')

    with open(indexFilePath, 'w') as indexFile:
        print('---', file=indexFile)
        print('bookToC: false', file=indexFile)
        print('bookCollapseSection: true', file=indexFile)
        print('title: ' + regelType, file=indexFile)
        print('description: Geconverteerd vanuit BizzDesign Excel-export', file=indexFile)
        print('---', file=indexFile)
        print('', file=indexFile)
        print('{{< sectiontable >}}', file=indexFile)

# write HUGO Markdown content Regel Files for regeType
def writeRegelsToHugoMdContentFiles(regelType: str, regels = []):
    for regel in regels:
        # write regel file
        regelPath = docsRegelPath(regelType)
        regelFile = regel.code.lower() + '.md'
        regelFilePath = os.path.join(regelPath, regelFile)
        with open(regelFilePath, 'w') as regelFile:
            print('---', file=regelFile)
            print('bookToC: false', file=regelFile)
            print('regelType: ' + regelType, file=regelFile)
            print('regelCode: ' + regel.code, file=regelFile)
            print('description: |\n  ' + txtValue(regel.name.split(regel.code + ': ')[1]), file=regelFile)
            print('wordtGebruiktIn: ' + txtValue(regel.wordtGebruiktIn), file=regelFile)
            if regel.geenTechnischeRegels != None:
                print('geenTechnischeRegels: ' + regel.geenTechnischeRegels, file=regelFile)
            if regel.XSDString != None:
                print('XSDString: ' + "'" + regel.XSDString + "'", file=regelFile)
            if regel.rationale != None:
                print('rationale: ' + regel.rationale, file=regelFile)
            print('---', file=regelFile)
            print('{{< hint warning >}}', file=regelFile)
            print('  ', file=regelFile)
            if regel.documentatie != None:
                print(regel.documentatie, file=regelFile)
            else:
                print('(geen documentatie)', file=regelFile)
            print('  ', file=regelFile)
            print('{{< /hint >}}', file=regelFile)

# convert BizDesign export Excel File with regels to HUGO Markdown Content Files
def convertBizXlsToHugoContent(regelExportFilePath: str):
    wb = load_workbook(filename=regelExportFilePath)
    ws = wb['Blad1']
    columnNames = []
    regels = []
    rowNr: int = 0
    # regeltype staat altijd in cell B2
    regelType: str = ws['B2'].value
    writeIndexFile(regelType)

    # process every row in the worksheet
    for wsRow in ws:
        if rowNr == 0:
            # add column names from the first row
            for wsCell in wsRow:
                columnNames.append(camelize(wsCell.value))
        elif rowNr > 1:
            # every row from third row contains data values
            columnNr = 0
            # create new iStandaard regel instance
            regel = IstdRegel()
            # process cell value for every column in the row
            for wsCell in wsRow:
                # map column cell values to regel instance
                columnName: str = columnNames[columnNr]
                if columnName == 'code':
                    regel.code = wsCell.value
                elif columnName == 'name':
                    regel.name = wsCell.value
                elif columnName == 'documentatie':
                    regel.documentatie = wsCell.value
                elif columnName == 'wordtGebruiktIn':
                    regel.wordtGebruiktIn = wsCell.value
                elif columnName == 'geenTechnischeRegels':
                    regel.geenTechnischeRegels = wsCell.value
                elif columnName == 'XSDString':
                    regel.XSDString = wsCell.value
                elif columnName == 'rationale':
                    regel.rationale = wsCell.value
                else:
                    print('ColumnName Not Mapped: ==> ' + columnName)
                # next column to process
                columnNr = columnNr + 1
            # add processed regel instance
            print(regel)
            regels.append(regel)
        # process next row
        rowNr = rowNr + 1

    writeRegelsToHugoMdContentFiles(regelType, regels)

# Main Conversion Script
inputPath = 'bizz-regel-exports/'

# process xlsx files
for inputFile in os.listdir(inputPath):
    if inputFile.endswith('.xlsx'):
        regelExportFilePath = os.path.join(inputPath, inputFile)
        print(regelExportFilePath)
        convertBizXlsToHugoContent(regelExportFilePath)
